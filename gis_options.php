<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// pour compat cf https://zone.spip.org/trac/spip-zone/changeset/79911/
define('_DIR_LIB_GIS', find_in_path('lib/leaflet/'));

$config = @unserialize($GLOBALS['meta']['gis']);
$api_key_bing = isset($config['api_key_bing']) ? trim($config['api_key_bing']) : '';

$gis_layers =  [
	'openstreetmap_mapnik' => [
		'nom' => 'OpenStreetMap',
		'layer' => 'L.tileLayer.provider("OpenStreetMap")'
	],
	'openstreetmap_de' => [
		'nom' => 'OpenStreetMap DE',
		'layer' => 'L.tileLayer.provider("OpenStreetMap.DE")'
	],
	'openstreetmap_fr' => [
		'nom' => 'OpenStreetMap FR',
		'layer' => 'L.tileLayer.provider("OpenStreetMap.France")'
	],
	'openstreetmap_hot' => [
		'nom' => 'OpenStreetMap H.O.T.',
		'layer' => 'L.tileLayer.provider("OpenStreetMap.HOT")'
	],
	'opentopomap' => [
		'nom' => 'OpenTopoMap',
		'layer' => 'L.tileLayer.provider("OpenTopoMap")'
	],
	'ign_plan' => [
		'nom' => 'IGN Carte',
		'layer' => 'L.tileLayer.provider("GeoportailFrance.")'
	],
	'ign_orthos' => [
		'nom' => 'IGN Ortho',
		'layer' => 'L.tileLayer.provider("GeoportailFrance.orthos")'
	],
	'cartodb_positron' => [
		'nom' => 'CartoDB Positron',
		'layer' => 'L.tileLayer.provider("CartoDB.Positron")'
	],
	'cartodb_positron_base' => [
		'nom' => 'CartoDB Positron Base',
		'layer' => 'L.tileLayer.provider("CartoDB.PositronNoLabels")'
	],
	'cartodb_darkmatter' => [
		'nom' => 'CartoDB DarkMatter',
		'layer' => 'L.tileLayer.provider("CartoDB.DarkMatter")'
	],
	'cartodb_darkmatter_base' => [
		'nom' => 'CartoDB DarkMatter Base',
		'layer' => 'L.tileLayer.provider("CartoDB.DarkMatterNoLabels")'
	],
	'cartodb_voyager' => [
		'nom' => 'CartoDB Voyager',
		'layer' => 'L.tileLayer.provider("CartoDB.Voyager")'
	],
	'cartodb_voyager_base' => [
		'nom' => 'CartoDB Voyager Base',
		'layer' => 'L.tileLayer.provider("CartoDB.VoyagerNoLabels")'
	],
	'stamen_toner' => [
		'nom' => 'Stamen Toner',
		'layer' => 'L.tileLayer.provider("Stamen.Toner")'
	],
	'stamen_tonerlite' => [
		'nom' => 'Stamen Toner Lite',
		'layer' => 'L.tileLayer.provider("Stamen.TonerLite")'
	],
	'stamen_terrain' => [
		'nom' => 'Stamen Terrain',
		'layer' => 'L.tileLayer.provider("Stamen.Terrain")'
	],
	'stamen_watercolor' => [
		'nom' => 'Stamen Watercolor',
		'layer' => 'L.tileLayer.provider("Stamen.Watercolor")'
	],
	'esri_worldstreetmap' => [
		'nom' => 'Esri WorldStreetMap',
		'layer' => 'L.tileLayer.provider("Esri.WorldStreetMap")'
	],
	'esri_delorme' => [
		'nom' => 'Esri DeLorme',
		'layer' => 'L.tileLayer.provider("Esri.DeLorme")'
	],
	'esri_worldtopomap' => [
		'nom' => 'Esri WorldTopoMap',
		'layer' => 'L.tileLayer.provider("Esri.WorldTopoMap")'
	],
	'esri_worldimagery' => [
		'nom' => 'Esri WorldImagery',
		'layer' => 'L.tileLayer.provider("Esri.WorldImagery")'
	],
	'esri_worldterrain' => [
		'nom' => 'Esri WorldTerrain',
		'layer' => 'L.tileLayer.provider("Esri.WorldTerrain")'
	],
	'esri_worldshadedrelief' => [
		'nom' => 'Esri WorldShadedRelief',
		'layer' => 'L.tileLayer.provider("Esri.WorldShadedRelief")'
	],
	'esri_worldphysical' => [
		'nom' => 'Esri WorldPhysical',
		'layer' => 'L.tileLayer.provider("Esri.WorldPhysical")'
	],
	'esri_oceanbasemap' => [
		'nom' => 'Esri OceanBasemap',
		'layer' => 'L.tileLayer.provider("Esri.OceanBasemap")'
	],
	'esri_natgeoworldmap' => [
		'nom' => 'Esri NatGeoWorldMap',
		'layer' => 'L.tileLayer.provider("Esri.NatGeoWorldMap")'
	],
	'esri_worldgraycanvas' => [
		'nom' => 'Esri WorldGrayCanvas',
		'layer' => 'L.tileLayer.provider("Esri.WorldGrayCanvas")'
	],
	'bing_aerial' => [
		'nom' => 'Bing Aerial',
		'layer' => 'L.BingLayer("' . $api_key_bing . '")'
	],
	'google_roadmap' => [
		'nom' => 'Google Roadmap',
		'layer' => 'L.gridLayer.googleMutant({type:"roadmap"})'
	],
	'google_satellite' => [
		'nom' => 'Google Satellite',
		'layer' => 'L.gridLayer.googleMutant({type:"satellite"})'
	],
	'google_terrain' => [
		'nom' => 'Google Terrain',
		'layer' => 'L.gridLayer.googleMutant({type:"terrain"})'
	]
];

if (isset($GLOBALS['gis_layers']) and is_array($GLOBALS['gis_layers'])) {
	$GLOBALS['gis_layers'] = array_merge($gis_layers, $GLOBALS['gis_layers']);
} else {
	$GLOBALS['gis_layers'] = $gis_layers;
}
