# Changelog

## [4.53.1] - 2022-07-11

### Fixed

- API cartes statiques, éviter de générer du vide quand certaines tuiles de la carte passent la date crossing line
- #27 Appel à suivre_invalideur conventionnel
- Update lib fullscreen pour compat ios
- Rétablir la compatibilité avec PHP < 7

## [4.53.0] - 2022-05-25

### Fixed

- Fix Warnings Undefined array key
- Compatibilité PHP 8 : éviter des warnings du type Deprecated: trim(): Passing null to parameter #1 ($string) of type string is deprecated

### Added

- Ajout d’un CHANGELOG
- #34 Ajout d'une API pour générer des cartes statiques

### Changed

- #40 Mise à jour de leaflet en 1.8.0 et des librairies utilisées par le plugin
